#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
  // Initialize the MPI environment. The two arguments to MPI Init are not
  // currently used by MPI implementations, but are there in case future
  // implementations might need the arguments.
  MPI_Init(NULL, NULL);

  // Get the number of processes
  int numprocs;
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  // Get the rank of the process
  int myid;
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int namelen;
  MPI_Get_processor_name(processor_name, &namelen);

  // Print off a hello world message
  printf("Hello world from processor %s, rank (Process ID) %d out of %d processors\n",
         processor_name, myid, numprocs);

  // Finalize the MPI environment. No more MPI calls can be made after this
  MPI_Finalize();
}
