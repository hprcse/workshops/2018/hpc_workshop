/*
 *	Master process
		 reads the data from a file into an array
		 broadcasts data array to other processes
	Compute processes (includes master)
	    search data array for value 11
    	    Notify other processes when they have found it
	    Other processes stop their search when they receive notificiation
 * * */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define MAXSIZE 1000000

int main(int argc, char **argv)
{
	int myid, numprocs;				// Process ID and Number of Processors	
	double startwtime, endwtime, totalTime; 	// Variable for estimating execution Time
	int namelen;					// Process name length
	long int s, s0, startIndex, endIndex;		// Variables for load sharing purpose
	long int  i;
	float data[MAXSIZE];		// Variable for Input Data, 
	float searchKey = 50.25;
	char fn[255]; 
	FILE *fp;
	int myfound, done;
	MPI_Status status;
  	MPI_Request request;

	char processor_name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv);				// Tells MPI Number of processors to use from prompt
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 	// Get the number of Processors available from MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);		// Get the ID allocated to the current node/processor
	MPI_Get_processor_name(processor_name, &namelen); // Get the current node/processor name and stored into an array of char from MPI

	fprintf(stderr, "Process %d is on %s\n", myid, processor_name);
	fflush(stderr);
	
	// Read Input from file data.txt
	if(0 == myid) {
		/* open input file and intialize data */
		strcpy(fn, getenv("PWD"));
		strcat(fn, "/data.txt");
		if( NULL == (fp = fopen(fn, "r")) ) {
			printf("Can't open the input file: %s\n\n", fn);
			exit(1);
		}
		for(i=0; i<MAXSIZE; i++) {
			fscanf(fp, "%f", &data[i]);
		}
	}
 
	if(myid == 0)
	{
		s = (int) floor(MAXSIZE/numprocs);	// Total number of elements/numbers for each worker (load of the worker)
		s0 = s + MAXSIZE%numprocs;		// Amount of numbers MASTER will Process
		printf("s=%d, s0=%d\n", s, s0);
	}

	/* broadcast data */
	MPI_Bcast(&s, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);	// Broadcast to ALL from Master's buffer to Worker
	MPI_Bcast(&s0, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

	startIndex = s0 + (myid - 1) *s;	// Masters size + (Node# -1) * Worker Size
	endIndex = startIndex + s;		// start + Worker size

	totalTime = 0;		// Initialize Total Execution time to 0
	if (myid == 0)
	{
		startwtime = MPI_Wtime();	// Only MASTER needs to keep track of execution Time
	}
	

	myfound=0;
	if (myid == 0)			// Master work
	{
		for(i=0; i < s0; i++)	// Search @ master's work
		{
			if(data[i] == searchKey)
			{
				dummy = 123;
				MPI_Send(&dummy,1,MPI_FLOAT,myid,86,MPI_COMM_WORLD);
				printf("P:%d found it at global index %d\n",myid,i);
				myfound = 1;
			}
			MPI_Test(&request,&done,&status);
		}
	}
	else						// Search @ Workers Work
	{
		for(i=startIndex; i<endIndex; i++)
		{
			if(data[i] == searchKey)
			{
				dummy = 123;
				MPI_Send(&dummy,1,MPI_FLOAT,myid,86,MPI_COMM_WORLD);
				printf("P:%d found it at global index %d\n",myid,i);
				myfound = 1;
			}
			MPI_Test(&request,&done,&status);
		}
	}
	
	MPI_Irecv(&dummy,1,MPI_INT,MPI_ANY_SOURCE,86,MPI_COMM_WORLD,&request);
  	MPI_Test(&request,&done,&status);

	if(myid==0) 
	{
		endwtime = MPI_Wtime();			// Get Current Wall Clock Time
		totalTime = endwtime - startwtime;	// Compute Execution Time
		
		if (!myfound) {
    			printf("P:%d stopped at global index %d\n",myid,i-1);
  		}
		printf("The Execution Time = %f sec\n",totalTime);	// Print Execution Time
	}

	MPI_Finalize();			// Exit the MPI programming environmemt
}
