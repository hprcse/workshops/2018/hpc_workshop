#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define MAXSIZE 1000000

int main(int argc, char **argv)
{
	int myid, numprocs;				// Process ID and Number of Processors	
	double startwtime, endwtime, totalTime; 	// Variable for estimating execution Time
	int namelen;					// Process name length
	long int s, s0, startIndex, endIndex;		// Variables for load sharing purpose
	long int  i;
	char fn[255];
	FILE *fp;
	int vector1[MAXSIZE+100];    		// Increase the vector size as scatter and gather is used.
	int vector2[MAXSIZE+100];
	int vector3[MAXSIZE+100];
	int part_sum, sum;

	char processor_name[MPI_MAX_PROCESSOR_NAME];

	MPI_Init(&argc, &argv);				// Tells MPI Number of processors to use from prompt
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 	// Get the number of Processors available from MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);		// Get the ID allocated to the current node/processor
	MPI_Get_processor_name(processor_name, &namelen); // Get the current node/processor name and stored into an array of char from MPI

	fprintf(stderr, "Process %d is on %s\n", myid, processor_name);
	fflush(stderr);

	/*	
	// Read Input from file data.txt
	if(0 == myid) {
		// open input file and intialize data 
		strcpy(fn, getenv("PWD"));
		strcat(fn, "/data.txt");
		if( NULL == (fp = fopen(fn, "r")) ) {
			printf("Can't open the input file: %s\n\n", fn);
			exit(1);
		}
		for(i=0; i<MAXSIZE; i++) {
			fscanf(fp, "%f", &data[i]);
		}
	}*/


	// Vector 1 Reading
   	for (i=0; i < MAXSIZE; i++)          
		vector1[i] = i; 
 	// Vector 2 Reading
    	for (i=0; i < MAXSIZE; i++)          
		vector2[i] = i;
 
	if(myid == 0)		//Add Zero to make all vectors are equally divisible by numprocs.
	{
		s = (int) floor(MAXSIZE/numprocs);	// Total number of elements/numbers for each worker (load of the worker)
		s0 = s + MAXSIZE%numprocs;		// Amount of numbers MASTER will Process
		printf("s=%d, s0=%d\n", s, s0);
		if (s0 != 0)
		{
			s = s + 1;
			for(i=0; i < ((s * numprocs) - ARRAY_SIZE); i++)
			{
				vector1[MAXSIZE + i] = 0;
				vector2[MAXSIZE + i] = 0;
			}
		}

	}
	
	sum=0;
	part_sum=0;
	if(myid == 0)		// Master Worker Work
	{
		MPI_Bcast(&s, 1, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Scatter(vector1, s, MPI_INT, vector1Receive, s, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Scatter(vector2, s, MPI_INT, vector2Receive, s, MPI_INT, 0, MPI_COMM_WORLD);
		for(i=0; i<s; i++)
		{
			vector3Receive[i] = vector1Receive[i] * vector2Receive[i];
			part_sum = part_sum + vector3Receive[i];
		}	
		MPI_Gather(vector3Receive, s, MPI_LONG, vector3, s, MPI_LONG, 0, MPI_COMM_WORLD);
	
		for(i=0; i<MAXSIZE; i++)			// Print the Vector1, Vector2 and Vector3
		{
			printf("%d %d %d %d\n",i,vector1[i],vector2[i], vector3[i]);
		}
	}
	else			// Workers Work
	{	
		MPI_Bcast(&MAXSIZE, 1, MPI_INT, 0, MPI_COMM_WORLD); 
		MPI_Bcast(&s, 1, MPI_INT, 0, MPI_COMM_WORLD);
		int vector1Receive[s];    
		int vector2Receive[s];
		int vector3Receive[s];
		MPI_Scatter(vector1, s, MPI_INT, vector1Receive, s, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Scatter(vector2, s, MPI_INT, vector2Receive, s, MPI_INT, 0, MPI_COMM_WORLD);
		for(i=0; i<s; i++)
		{
			vector3Receive[i] = vector1Receive[i] * vector2Receive[i];	// Partial vector Multiplication
			part_sum = part_sum + vector3Receive[i];
		}	
		MPI_Gather(vector3Receive, s, MPI_LONG, vector3, s, MPI_LONG, 0, MPI_COMM_WORLD);

	}
	MPI_Reduce(&part_sum, &sum, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	if(myid==0) 
	{
		endwtime = MPI_Wtime();			// Get Current Wall Clock Time
		totalTime = endwtime - startwtime;	// Compute Execution Time
		printf("Vector Dot Product is = %ld\n", sum);
		printf("The Execution Time = %f sec\n",totalTime);	// Print Execution Time
	}

	MPI_Finalize();
}


