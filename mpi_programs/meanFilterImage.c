#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define NR 386                /* number of rows  */
#define NC 514                /* number of columns */
#define MASTER 0               /* taskid of first task */
#define FROM_MASTER 1          /* setting a message type */
#define FROM_WORKER 2          /* setting a message type */

int main (int argc, char *argv[])
{
int	numtasks,              /* number of tasks in partition */
	taskid,                /* a task identifier */
	numworkers,            /* number of worker tasks */
	source,                /* task id of message source */
	dest,                  /* task id of message destination */
	mtype,                 /* message type */
	rows,                  /* rows of matrix A sent to each worker */
	averow, extra, offset, /* used to determine rows sent to each worker */
	i, j, k, rc;           /* misc */         
MPI_Status status;

long double mred[NR-2][NC-2],mblue[NR-2][NC-2],mgreen[NR-2][NC-2],red[NR][NC],green[NR][NC],blue[NR][NC];
   
MPI_Init(&argc,&argv);
MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
if (numtasks < 2 ) {
  printf("Need at least two MPI tasks. Quitting...\n");
  MPI_Abort(MPI_COMM_WORLD, rc);
  exit(1);
  }
numworkers = numtasks-1;


/**************************** master task ************************************/
   if (taskid == MASTER)
   {
      //printf("mpi_mm has started with %d tasks.\n",numtasks);
      //printf("Initializing arrays...\n");
      FILE *fpr,*fpb,*fpg;
      fpr = fopen("red.txt","r");
      fpb = fopen("blue.txt","r");
      fpg = fopen("green.txt","r");
      for( i=0;i<NR;i++)
      {
		red[i][0]=0;
		blue[i][0]=0;
		green[i][0]=0;
		red[i][NC-1]=0;
		blue[i][NC-1]=0;
		green[i][NC-1]=0;
      }
	for( i=0;i<NC;i++)
      {
		red[0][i]=0;
		blue[0][i]=0;
		green[0][i]=0;
		red[NR-1][i]=0;
		blue[NR-1][i]=0;
		green[NR-1][i]=0;
      }	
      for( i=1;i<NR-1;i++)
      {
         for( j=1;j<NC-1;j++)
         {
            fscanf( fpr,"%Lf",&red[i][j] );
            fscanf( fpb,"%Lf",&blue[i][j]);
            fscanf( fpg,"%Lf",&green[i][j]);
		//red[i][j]=1;
		//blue[i][j]=1;
		//green[i][j]=1;
         }
      }
      fclose(fpr);
      fclose(fpg);
      fclose(fpb);
      /* Send matrix data to the worker tasks */
      averow = (NR-2)/numworkers;
      extra = (NR-2)%numworkers;
      offset = 1;
      mtype = FROM_MASTER;
      for (dest=1; dest<=numworkers; dest++)
      {
         rows = (dest <= extra) ? averow+1 : averow;     
         //printf("Sending %d rows to task %d offset=%d\n",rows,dest,offset);
         MPI_Send(&offset, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&rows, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&red[offset-1][0], (rows+2)*NC, MPI_LONG_DOUBLE, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&blue[offset-1][0], (rows+2)*NC, MPI_LONG_DOUBLE, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&green[offset-1][0], (rows+2)*NC, MPI_LONG_DOUBLE, dest, mtype, MPI_COMM_WORLD);
         offset = offset + rows;
      }

      /* Receive results from worker tasks */
      mtype = FROM_WORKER;
      for (i=1; i<=numworkers; i++)
      {
         source = i;
         MPI_Recv(&offset, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&rows, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&mred[offset-1][0], rows*(NC-2), MPI_LONG_DOUBLE, source, mtype,MPI_COMM_WORLD, &status);
         MPI_Recv(&mblue[offset-1][0], rows*(NC-2), MPI_LONG_DOUBLE, source, mtype,MPI_COMM_WORLD, &status);
         MPI_Recv(&mgreen[offset-1][0], rows*(NC-2), MPI_LONG_DOUBLE, source, mtype,MPI_COMM_WORLD, &status);
         //printf("Received results from task %d\n",source);
      }

      /* Print results */
      printf("******************************************************\n");
      printf("Red mean matrix:\n");
      for (i=0; i<NR-2; i++)
      {
         printf("\n"); 
         for (j=0; j<NC-2; j++) 
            printf("%6.2Lf   ", mred[i][j]);
      }
      printf("\n******************************************************\n");
      printf("******************************************************\n");
      printf("Blue mean matrix:\n");
      for (i=0; i<NR-2; i++)
      {
         printf("\n"); 
         for (j=0; j<NC-2; j++) 
            printf("%6.2Lf   ", mblue[i][j]);
      }
      printf("\n******************************************************\n");
      printf("******************************************************\n");
      printf("Green mean matrix:\n");
      for (i=0; i<NR-2; i++)
      {
         printf("\n"); 
         for (j=0; j<NC-2; j++) 
            printf("%6.2Lf   ", mgreen[i][j]);
      }
      printf("\n******************************************************\n");
      
      printf ("Done.\n");
   }


/**************************** worker task ************************************/
   if (taskid > MASTER)
   {
      mtype = FROM_MASTER;
      MPI_Recv(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&red, (rows+2)*NC, MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&blue, (rows+2)*NC, MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&green, (rows+2)*NC, MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);

      for (i=1; i<rows+1; i++)
         for (k=1; k<NC-1; k++)
         {
            mred[i-1][k-1] = (red[i-1][k-1]+red[i-1][k]+red[i-1][k+1]+red[i][k-1]+red[i][k]+red[i][k+1]+red[i+1][k-1]+red[i+1][k]+red[i+1][k+1])/9;
            mblue[i-1][k-1] = (blue[i-1][k-1]+blue[i-1][k]+blue[i-1][k+1]+blue[i][k-1]+blue[i][k]+blue[i][k+1]+blue[i+1][k-1]+blue[i+1][k]+blue[i+1][k+1])/9;
            mgreen[i-1][k-1] = (green[i-1][k-1]+green[i-1][k]+green[i-1][k+1]+green[i][k-1]+green[i][k]+green[i][k+1]+green[i+1][k-1]+green[i+1][k]+green[i+1][k+1])/9;
         }

      mtype = FROM_WORKER;
      MPI_Send(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&mred, rows*(NC-2), MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&mblue, rows*(NC-2), MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&mgreen, rows*(NC-2), MPI_LONG_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
   }
   MPI_Finalize();
}

