#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define n 384
#define m 512

int main()
{
	int red[n][m],green[n][m],blue[n][m],mred[n][m],mblue[n][m],mgreen[n][m],i,j;
	float start,end,exec_time;
	FILE *fpr,*fpb,*fpg,*fpmg,*fpmb,*fpmr;
	fpr = fopen("red.txt","r");
	fpb = fopen("blue.txt","r");
	fpg = fopen("green.txt","r");
	for( i=0;i<n;i++)
	{
		for( j=0;j<m;j++)
		{
			fscanf( fpr,"%d",&red[i][j] );
			fscanf( fpb,"%d",&blue[i][j]);
			fscanf( fpg,"%d",&green[i][j]);
			mred[i][j] = red[i][j];
			mblue[i][j] = blue[i][j];
			mgreen[i][j] = green[i][j];
		}
	}
	fclose(fpr);
	fclose(fpg);
	fclose(fpb);
	start = omp_get_wtime();
	#pragma omp parallel sections 
	{
		#pragma omp section	
		{
			for( i=1; i<n-1;i++ )
			{
				for( j=1;j<n-1;j++ )
				{
					mred[i][j] = red[i-1][j-1]+red[i-1][j]+red[i+1][j]+red[i][j-1]+red[i][j]+red[i][j+1]+red[i+1][j-1]+red[i+1][j]+red[i+1][j+1];
					mred[i][j] = mred[i][j]/9;
				}
			}
		}
		#pragma omp section
		{
			for( i=1; i<n-1;i++ )
			{
				for( j=1;j<n-1;j++ )
				{
					mblue[i][j] = blue[i-1][j-1]+blue[i-1][j]+blue[i+1][j]+blue[i][j-1]+blue[i][j]+blue[i][j+1]+blue[i+1][j-1]+blue[i+1][j]+blue[i+1][j+1];
					mblue[i][j] = mblue[i][j]/9;
				}
			}
		}
		#pragma omp section
		{
			for( i=1; i<n-1;i++ )
			{
				for( j=1;j<n-1;j++ )
				{
					mgreen[i][j] = green[i-1][j-1]+green[i-1][j]+green[i+1][j]+green[i][j-1]+green[i][j]+green[i][j+1]+green[i+1][j-1]+green[i+1][j]+green[i+1][j+1];
					mgreen[i][j] = mgreen[i][j]/9;
				}
			}
		}
	}
	end = omp_get_wtime();
	exec_time = end - start;
	printf("%f\n",exec_time);	

	return(0);
}
