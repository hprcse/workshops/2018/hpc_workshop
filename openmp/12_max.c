#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#define n 10

int main()
{
	int a[n];
	int globalMax, localMax;
	float startTime, endTime,execTime;
	int i;
	int omp_rank;

		srand(time(0));
	#pragma omp parallel for
	for( i = 0; i < n;i++)
	{
		a[i] = rand() % 100;
		printf("%d ",a[i]);
	}

	globalMax = a[0];
	startTime = omp_get_wtime();
	#pragma omp parallel private (i,localMax) shared (a, globalMax) 
	{
		localMax = a[0];
		#pragma omp  for 
		for(i=0;i<n;i++)
		{
			omp_rank = omp_get_thread_num();
			if(a[i] > localMax)  	// Use Random function and assign a[i]
				localMax = a[i];
			printf("The value of a[%d] = %d and localMax = %d done by worker Thread ID = %d\n", i, a[i], localMax, omp_rank);
		}
		#pragma omp critical(dosum)		
		{
			//omp_rank = omp_get_thread_num();
			if(globalMax < localMax)
				globalMax = localMax;
			printf("Global Minimum = %d\n", globalMax);
		}

	}
	endTime = omp_get_wtime();

	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
