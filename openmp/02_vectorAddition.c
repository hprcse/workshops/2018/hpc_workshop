#include <stdio.h>
#include <omp.h>

#define n 100000

int main()
{
	double a[n],b[n], c[n];
	float startTime, endTime,execTime;
	int i;
	int omp_rank;
	
	startTime = omp_get_wtime();
	#pragma omp parallel private (i) shared (a,b,c)
	{	
		#pragma omp for	
		for(i=0;i<n;i++)
		{
			omp_rank = omp_get_thread_num();
			a[i] = i * 10.236;  	// Use Random function and assign a[i]
			b[i] = i * 152.123;	// Use Random function and assign b[i]
			c[i] = a[i] + b[i];
			printf("The value of a[%d] = %lf and b[%d] = %lf and result c[%d] = %lf done by worker Thread ID = %d\n", i, a[i], i, b[i], i, c[i], omp_rank);
		}		
	
	}
	endTime = omp_get_wtime();

	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
