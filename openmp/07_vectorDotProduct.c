#include <stdio.h>
#include <omp.h>

#define n 100000

int main()
{
	double a[n],b[n], psum, sum;
	float startTime, endTime,execTime;
	int i;
	int omp_rank;
	
	sum = 0;
	startTime = omp_get_wtime();
	#pragma omp parallel private (i,psum) shared (a,b, sum)
	{	
		psum=0;
		#pragma omp for
		for(i=0;i<n;i++)
		{	
			omp_rank = omp_get_thread_num();
			a[i] = i * 10.236;  	// Use Random function and assign a[i]
			b[i] = i * 152.123;	// Use Random function and assign b[i]
			psum = psum + a[i] * b[i];
			printf("The value of a[%d] = %f and b[%d] = %f and result psum = %f done by worker Thread ID = %d\n", i, a[i], i, b[i], psum, omp_rank);
		}		
		#pragma omp critical
		{
			omp_rank = omp_get_thread_num();
			sum = sum + psum;
			printf("The final DOT product is %f computed using Thread ID = %d\n", sum, omp_rank);
		}
	}
	endTime = omp_get_wtime();

	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
