#include <stdio.h>
#include <omp.h>

#define n 100000

int main()
{
	double a[n]={698.79667},b[n];
	float startTime, endTime, execTime;
	int i;
	int omp_rank;

	startTime = omp_get_wtime();
	#pragma omp parallel private (i) shared (a,b)
	{
		omp_rank = omp_get_thread_num();
		#pragma omp for	
		for(i=0;i<n;i++)
		{
			b[i] = (i+253)*a[i]-658.7;		
			printf("The Value b[%d] is %f is computed in Iteration number i = %d and computed by Thread Id = %d\n", i, b[i],i,omp_rank);
		}
	}
	endTime = omp_get_wtime();

	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
