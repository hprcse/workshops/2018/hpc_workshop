#include <stdio.h>
#include <omp.h>

#define n 250
//row major
int main()
{
	int i,j;
	double m1[n][n]={8734.463},m2[n][n]={7634.324},m3[n][n];
	float startTime, endTime, execTime;
	int omp_rank;

	startTime = omp_get_wtime();
	#pragma omp parallel private (i,j) shared (m1,m2,m3)
	{
		#pragma omp for
		for(i=0 ; i< n ; i++)
		{	
			omp_rank = omp_get_thread_num();
			for(j=0 ; j< n ;j++)
			{	
				m3[i][j] = 0;
				for(int k=0;k< n; k++)
				{
					m3[i][j] = m3[i][j] + m1[i][k] * m2[k][j];
				}
				printf("The value m3[%d][%d] is %f is computed by iteration i=%d and j=%d using the Thread Id = %d\n",i,j,m3[i][j],i,j,omp_rank);
			}
		}
	}

	endTime = omp_get_wtime();
	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
