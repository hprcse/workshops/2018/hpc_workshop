#include <stdio.h>
#include <omp.h>

#define n 250

int main()
{
	long double m1[n][n]={98.65},m2[n][n]={34.87},m3[n][n];
	float startTime,endTime,execTime;
	int i,j;
	int omp_rank;

	startTime = omp_get_wtime();
	#pragma omp parallel private (i,j) shared (m1,m2,m3)
	{
		omp_rank = omp_get_thread_num();
		#pragma omp for
		for(i=0;i<250;i++)
		{
			for(j=0;j<250;j++)
			{
				m3[i][j] = m1[i][j] + m2[i][j];
				printf("The Value i = %d and Value j = %d m3[%d][%d] is computed by Tread ID is %d\n",i, j, i, j, omp_rank);
			}
		}
	}
	endTime = omp_get_wtime();

	execTime = endTime-startTime;
	printf("%f \n",execTime);
	return(0);
}
