#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#define n 10

int main()
{
	int a[n];
	int globalMin, localMin;
	float startTime, endTime,execTime;
	int i;
	int omp_rank;

		srand(time(0));
	#pragma omp parallel for
	for( i = 0; i < n;i++)
	{
		a[i] = rand() % 100;
		printf("%d ",a[i]);
	}

	globalMin = a[0];
	startTime = omp_get_wtime();
	#pragma omp parallel private (i,localMin) shared (a, globalMin) 
	{
		localMin = a[0];
		#pragma omp  for 
		for(i=0;i<n;i++)
		{
			omp_rank = omp_get_thread_num();
			if(a[i] < localMin)  	// Use Random function and assign a[i]
				localMin = a[i];
			printf("The value of a[%d] = %d and localMin = %d done by worker Thread ID = %d\n", i, a[i], localMin, omp_rank);
		}
		#pragma omp critical(dosum)		
		{
			//omp_rank = omp_get_thread_num();
			if(globalMin > localMin)
				globalMin = localMin;
			printf("Global Minimum = %d\n", globalMin);
		}

	}
	endTime = omp_get_wtime();

	execTime = endTime - startTime;
	printf("%f \n",execTime);
	return(0);
}
